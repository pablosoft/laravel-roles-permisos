@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">List of Roles</div>

                    <div class="card-body">
                        <a href="{{ route('role.create') }}" class="btn btn-primary float-right">Crear</a>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Full access</th>
                                    <th colspan="3"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach ($roles as $role)
                                        <th scope="row">{{ $role->id }}</th>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->slug}}</td>
                                    <td>{{ $role->description}}</td>
                                    <td>{{ $role['full-access']}}</td>
                                    <td><a class="btn btn-info" href="{{ route('role.show',$role->id) }}">Show</a></td>
                                    <td><a class="btn btn-success" href="{{ route('role.show',$role->id) }}">Edit</a></td>
                                    <td><a class="btn btn-danger" href="{{ route('role.show',$role->id) }}">Delete</a></td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

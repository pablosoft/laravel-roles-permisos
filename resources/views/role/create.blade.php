@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">List of roles</div>

                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif

                      <form action="{{ route('role.store') }}" method="POST">
                          @csrf
                          <div class="container">
                              <h3>Require Data</h3>
                          </div>
                          <div class="form-group">
                              <label for="exampleFormControlInput1">Email address</label>
                              <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

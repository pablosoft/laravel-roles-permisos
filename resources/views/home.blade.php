@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List of roles</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido :D
                </div>
                <div class="card">
                    <div class="body">
                        <a class="btn btn-info" href="{{route('role.index')}}">Ver roles</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\PabloPermission\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //es: desde aqui
    //en: from here
    protected $fillable = [
        'name', 'slug', 'description', 'full-access',
    ];

    //es:de muchos a muchos
    //en:many to many
    public function users(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function permissions(){
        return $this->belongsToMany('App\PabloPermission\Models\Permission')->withTimestamps();
    }
}
